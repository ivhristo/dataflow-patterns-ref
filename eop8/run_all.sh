#/usr/bin/env bash 

# run: ./run_all.sh /home/ppd/lmo86446/issue_eop7  /home/ppd/lmo86446/issue_eop8/dataflow-software
# run: ./run_all.sh /home/ppd/lmo86446/issue_eop7 /home/ppd/lmo86446/private/test/22

bindir=$1 # /home/ppd/lmo86446/issue_eop7
hfdir=$2  # /home/ppd/lmo86446/private/test/22

source ${bindir}/dataflow-software/env_sim.sh

# TODO: fix hitFormatter.py for G
for p in A B C D E F # G
  do 
    ./hf.sh FixedHits_${p} /home/ppd/lmo86446/issue_eop7 /home/ppd/lmo86446/private/test/22
  done

# TODO: fix hitFormatter.py for F
for p in A B C D E # F 
  do
    ./hf.sh UniqueHits_${p} /home/ppd/lmo86446/issue_eop7 /home/ppd/lmo86446/private/test/22
  done
