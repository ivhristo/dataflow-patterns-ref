#/usr/bin/env bash

# run: ./hf.sh FixedHits_A /home/ppd/lmo86446/issue_eop7 /home/ppd/lmo86446/private/test/22 

PATTERN=$1  # FixedHits_A 
bindir=$2   # /home/ppd/lmo86446/issue_eop7
hfdir=$3  # /home/ppd/lmo86446/private/test/22

for i in {0..3} 
  do 
    python ${bindir}/dataflow-software/bin/hitFormatter.py -i ${hfdir}/${PATTERN}_eop8_hf_eop_axi4s.${i}.txt --intype axi4s --view hit >> ${PATTERN}_hitfile_${i}.txt
  done
