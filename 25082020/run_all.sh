#/usr/bin/env bash 

# run: ./run_all.sh /home/ppd/lmo86446/tpg_pedchanfix_branch /home/ppd/lmo86446/private/test/26

bindir=$1 # /home/ppd/lmo86446/tpg_pedchanfix_branch
hfdir=$2  # /home/ppd/lmo86446/private/test/26

source ${bindir}/dataflow-software/env_sim.sh

for p in A B C D E F G
  do 
    ./hf.sh FixedHits_${p} ${bindir} ${hfdir}
  done

for p in A B C D E F 
  do
    ./hf.sh UniqueHits_${p} ${bindir} ${hfdir}
  done
