#/usr/bin/env bash 

branch=$1
type=$2
subdir=$3

echo "PWD -> "
pwd=$(pwd)  # /builds/ivhristo/dataflow-patterns-ref
echo ${pwd}

cd ..
echo "PWD ->"
echo $(pwd)
#git clone -b ${branch} https://gitlab.cern.ch/DUNE-SP-TDR-DAQ/dataflow-software.git
cd ${pwd}
echo "PWD ->"
echo $(pwd)


for ff in $(ls ./${subdir}/${type}_*)
  do 
    f=$(basename "${ff}")
    #echo ${f}
    #echo $(ls ../dataflow-software/${f})
    echo "diff ./${subdir}/${f} ../dataflow-software/${f}"
    #diff ${f} ../dataflow-software/${f}
    diff ./${subdir}/${f} ../dataflow-software/${f}
  done
